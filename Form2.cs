﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Diagnostics;
using System.Xml;
 

namespace MWCC_CSharp
{
    public partial class Form2 : Form
    {
        //List<Order> Orders = new List<Order>();
        System.Data.DataTable table = new System.Data.DataTable();
        DataRow row;
        DataColumn column = new DataColumn();

        List<Item> Items = new List<Item>();        
        string region;
        FileInfo file;
                
        DateTime starttime = Process.GetCurrentProcess().StartTime;
    
        //Excel
        Microsoft.Office.Interop.Excel.Application excel = new Microsoft.Office.Interop.Excel.Application();
        Workbook workbook;
        Worksheet sheet;
        Range range;
        int linecount;

        //Order Variables
        Order order = new Order();
        string ordernum;
        string ponum;
        string location;
        string loccode;
        string orderdate;
        string orderby;

        //Item Class Variables
        int i;        
        int lineid;
        string name;
        string sku;
        string uom;
        string unit;
        string qty;
        string cost;

        int c;
        int column_sku;
        int column_qty = 1;
        int column_desc;
        int column_cost;
        int column_total;

        string[] casesku = { "50110", "50130", "42330", "36123" };
        string[] case24 = { "72600", "72610", "72620", "72640", "72650", "72690", "72700", "66605", "66630", "66625", "66620", "66655", "66665","68700","68800","68900" };
        string[] packsku = { "36533","36593", "36014", "36494", "36497", "36500", "36001", "36004", "41884", "41880", "42325", "36024" };

        public Form2()
        {
            InitializeComponent();
        }
        //Function to open Minnesota order files
        //public object openMNFile(FileInfo file)
        public object openFile(FileInfo file)
        {
            workbook = excel.Workbooks.Open(file.FullName);
            sheet = workbook.Worksheets.Item[1];
            range = sheet.Range["A9", "F192"];
            linecount = range.Rows.Count;
            Item item = new MWCC_CSharp.Item();

            //-----------------Order Data---------------------------
            ordernum = (sheet.Cells.Item[2, 4].Text).Replace(".", "");
            location = sheet.Cells.Item[3, 2].Text;
            loccode = (file.Name).Split('_')[0];
            ponum = (loccode + (sheet.Cells.Item[4, 2].Text).Replace(".", ""));
            if (ordernum.Equals("")) { ordernum = ponum; }
            orderby = sheet.Cells.Item[4, 4].Text;
            orderdate = (sheet.Cells.Item[5, 4].Text).Replace(".", "-");
            textBox8.Text = sheet.Cells.Item[194, 6].Text;

            order.Add(ordernum, location, loccode, ponum, orderby, orderdate);

            textBox1.Text = location;
            textBox2.Text = ordernum;
            textBox3.Text = loccode;
            textBox4.Text = ponum;
            textBox5.Text = orderdate;
            textBox6.Text = orderby;
            //------------------------------------------------------

            //------------------Item Data---------------------------
            for (i = 1; i < linecount; i++)
            {
                lineid = i + 8;
                name = (range.Cells.Item[i, 2].Text).TrimStart();
                sku = range.Cells.Item[i, 1].Text;
                if (sku.Length == 3) { sku = sku.PadLeft(4, '0'); }
                unit = range.Cells.Item[i, 5].Text;
                if (lineid < 169 || (casesku.Contains<string>(sku))) { uom = "CS"; }
                else if (unit != "1") { uom = "PK"; }
                else { uom = "EA"; }
                qty = range.Cells.Item[i, 4].Text;
                cost = (range.Cells.Item[i, 6].Text).Replace(" ", "");
                if (sku.Equals("36125")) { cost = "$0.00"; }

                item.Add(lineid, ordernum, name, sku, uom, unit, qty, cost);

            //if ((item.GetSKU != "Product #") && (item.GetQty != "0" && item.GetQty != "") )
               if (item.GetQty.Length <3 && item.GetQty.Length >= 1 && item.GetQty != "0")
                {
                    row = table.NewRow();
                    row["Line"] = item.GetLineID;
                    row["OrderNum"] = ordernum;
                    row["Name"] = item.GetName;
                    row["SKU"] = item.GetSKU;
                    row["UOM"] = item.GetUOM;
                    row["Qty"] = item.GetQty;
                    row["Cost"] = item.GetCost;
                    table.Rows.Add(row);
                }                
            }
            //----------------------------------------------------

            //--------------Populate Data Grid--------------------
            dataGridView1.DataSource = table;
            //----------------------------------------------------

            return order;            

        }

        /*public object openCAFile(FileInfo file)
        {
            workbook = excel.Workbooks.Open(file.FullName);
            sheet = workbook.Worksheets.Item[1];
            range = sheet.Range["A14", "J118"];
            linecount = range.Rows.Count;
            Item item = new MWCC_CSharp.Item();          
                    
            //-----------------Order Data---------------------------
                        
            location = (sheet.Cells.Item[1, 1].Text).Split(',')[0];
            ordernum = (file.Name).Split('_')[1];
            loccode = (file.Name).Split('_')[0];
            ponum = (loccode + ordernum);
            string dt = (((file.Name).Split('_')[2]).Split('.')[0].Insert(2,"/")).Insert(5,"/");
            if (ordernum.Equals("")) { ordernum = ponum; }

            //Find Date cell
            for (int d = 1; d < 10; d++)
            {
                if ((sheet.Cells.Item[d,1].Text).Equals("Date"))
                {
                    orderdate = sheet.Cells.Item[d + 1, 1].Text;
                       
                }
            }                       

            order.Add(ordernum, location, loccode, ponum, orderby, orderdate);

            textBox1.Text = location;
            textBox2.Text = ordernum;
            textBox3.Text = loccode;
            textBox4.Text = ponum;
            textBox5.Text = orderdate;
            textBox6.Text = "N/A";
            //------------------------------------------------------

            //------------------Item Data---------------------------

            //Separating pages within Excel worksheet by Qty column header
            List<int> lines = new List<int>();
                        
            for (int r = 1; r < 200; r++)
            {
                if ((sheet.Cells.Item[r, 1].Text).Equals("Qty"))
                {
                    lines.Add(r);
                }
            }
            foreach (int line in lines)
            {
                

                for (c = 1; c < 15; c++)
                {
                    if ((sheet.Cells.Item[line, c].Text).Contains("Item")) { column_sku = c; }
                    if ((sheet.Cells.Item[line, c].Text).Equals("Description")) { column_desc = c; }
                    if ((sheet.Cells.Item[line, c].Text).Equals("Amount")) { column_cost = c; }
                    if ((sheet.Cells.Item[line + 28, c].Text).StartsWith("Total")) { column_total = c; }

                }
                for (i = line + 1; i < line + 27; i++)

                {
                    
                    lineid = i;                    
                    name = (sheet.Cells.Item[i, column_desc].Text);
                    sku = sheet.Cells.Item[i, column_sku].Text;
                    if (sku.Length == 3) { sku = sku.PadLeft(4, '0'); } //Adding leading '0' removed by Excel    
                    //Determining unit of measure based on SKU                
                    if (packsku.Contains<string>(sku)) { uom = "PK"; }
                        else if (sku.Equals("36283")) { uom = "EA"; }
                        else { uom = "CS"; }
                    qty = sheet.Cells.Item[i, column_qty].Text;
                    //Quantities are presented in EA, dividing by 12 for CS
                    if (uom.Equals("CS") && qty != "")
                        {
                            if (case24.Contains<string>(sku)) { qty = (Int32.Parse(qty) / 24).ToString(); } //Items that have case qty of 24                            
                                else { qty = (Int32.Parse(qty) / 12).ToString(); }
                        }
                    cost = sheet.Cells.Item[i, column_cost].Text;

                    if (qty.Equals("")) { break; } //End of order

                    item.Add(lineid, ordernum, name, sku, uom, unit, qty, cost);


                    if (item.GetCost != "Amount")
                    {
                        row = table.NewRow();
                        row["Line"] = item.GetLineID;
                        row["OrderNum"] = ordernum;
                        row["Name"] = item.GetName;
                        row["SKU"] = item.GetSKU;
                        row["UOM"] = item.GetUOM;
                        row["Qty"] = item.GetQty;
                        row["Cost"] = item.GetCost;
                        table.Rows.Add(row);
                    }
                    
                }
            }

            //Finding Total cell
            for(int t = 1; t < 30; t++)
            {
                if ((sheet.Cells.Item[lines[(lines.Count - 1)] + t, column_total].Text).Contains("Total"))
                {
                    textBox8.Text = "$" + (sheet.Cells.Item[lines[(lines.Count - 1)] + t, column_total].Text).Split('$')[1];
                }
            }            

            //----------------------------------------------------

            //Output order to DataGrid

                dataGridView1.DataSource = table;

                return order;
            }           

        public object openTXFile(FileInfo file)
        {
            workbook = excel.Workbooks.Open(file.FullName);
            sheet = workbook.Worksheets.Item[1];
            //range = sheet.Range["A9", "F196"];
            //linecount = range.Rows.Count;
            Item item = new MWCC_CSharp.Item();

            //-----------------Order Data---------------------------
            ordernum = sheet.Cells.Item[2, 1].Text;
            //location = sheet.Cells.Item[3, 2].Text;
            loccode = (file.Name).Split('_')[0];
            ponum = ordernum;            
            //orderby = sheet.Cells.Item[4, 4].Text;
            orderdate = sheet.Cells.Item[4, 1].Text;
            textBox8.Text = sheet.Cells.Item[197, 6].Text;

            order.Add(ordernum, location, loccode, ponum, orderby, orderdate);

            textBox1.Text = location;
            textBox2.Text = ordernum;
            textBox3.Text = loccode;
            textBox4.Text = ponum;
            textBox5.Text = orderdate;
            textBox6.Text = orderby;
            //------------------------------------------------------

            //------------------Item Data---------------------------

            List<int> lines = new List<int>();
            for (int r = 1; r< sheet.UsedRange.Rows.Count; r++)
            {
                if ((sheet.Cells.Item[r, 1].Text).Contains("Item"))
                {
                    lines.Add(r);
                }
            }

            foreach (int line in lines)
            {
                i = 1;
                int l;
                string product = (sheet.Cells.Item[(line + i), 3].Text);
                string[] proddata = product.Split(' ');
                int pcount = proddata.Length;
                bool result = Int32.TryParse((sheet.Cells.Item[(line + i), 1].Text), out l);
                while (result == true)
                {
                    lineid = l;
                    
                    for (int p = 0; p < proddata.GetUpperBound(0); p++)
                    {
                        name = name + proddata[p];
                    }                    
                    sku = proddata[proddata.GetUpperBound(0)];

                    if (sku.Length == 3) { sku = sku.PadLeft(4, '0'); }
                    unit = sheet.Cells.Item[i, 5].Text;
                    if (lineid < 169 || (casesku.Contains<string>(sku))) { uom = "CS"; }
                    else if (unit != "1") { uom = "PK"; }
                    else { uom = "EA"; }
                    qty = range.Cells.Item[i, 4].Text;
                    cost = (range.Cells.Item[i, 6].Text).Replace(" ", "");
                    if (sku.Equals("36125")) { cost = "$0.00"; }

                    item.Add(lineid, ordernum, name, sku, uom, unit, qty, cost);

                    //if ((item.GetSKU != "Product #") && (item.GetQty != "0" && item.GetQty != "") )
                    if (item.GetQty.Length < 3 && item.GetQty.Length >= 1 && item.GetQty != "0")
                    {
                        row = table.NewRow();
                        row["Line"] = item.GetLineID;
                        row["OrderNum"] = ordernum;
                        row["Name"] = item.GetName;
                        row["SKU"] = item.GetSKU;
                        row["UOM"] = item.GetUOM;
                        row["Qty"] = item.GetQty;
                        row["Cost"] = item.GetCost;
                        table.Rows.Add(row);
                    }

                }
            }
            
            for (i = 1; i < sheet.UsedRange.Rows.Count; i++)
            {
                lineid = i + 8;
                name = (range.Cells.Item[i, 2].Text).TrimStart();
                sku = range.Cells.Item[i, 1].Text;
                if (sku.Length == 3) { sku = sku.PadLeft(4, '0'); }
                unit = range.Cells.Item[i, 5].Text;
                if (lineid < 169 || (casesku.Contains<string>(sku))) { uom = "CS"; }
                else if (unit != "1") { uom = "PK"; }
                else { uom = "EA"; }
                qty = range.Cells.Item[i, 4].Text;
                cost = (range.Cells.Item[i, 6].Text).Replace(" ", "");
                if (sku.Equals("36125")) { cost = "$0.00"; }

                item.Add(lineid, ordernum, name, sku, uom, unit, qty, cost);

                //if ((item.GetSKU != "Product #") && (item.GetQty != "0" && item.GetQty != "") )
                if (item.GetQty.Length < 3 && item.GetQty.Length >= 1 && item.GetQty != "0")
                {
                    row = table.NewRow();
                    row["Line"] = item.GetLineID;
                    row["OrderNum"] = ordernum;
                    row["Name"] = item.GetName;
                    row["SKU"] = item.GetSKU;
                    row["UOM"] = item.GetUOM;
                    row["Qty"] = item.GetQty;
                    row["Cost"] = item.GetCost;
                    table.Rows.Add(row);
                }
            }
            //----------------------------------------------------


            return order;
        }*/

        
        public void ExportXML(System.Data.DataTable table)
        {
            SaveFileDialog save = new SaveFileDialog();
            save.DefaultExt = "xml";
            save.InitialDirectory = "\\\\MED-FS01\\SHARE\\MWCC_Orders_for_Nav\\";
            save.OverwritePrompt = true;
            save.AddExtension = true;
            save.FileName = order.GetLocation + "_" + order.GetLocationCode + "_" + order.GetPONum;
            save.Filter = "*.xml|XML Files";
            save.ShowDialog();

            XmlWriter xmlwrite = XmlWriter.Create(save.FileName);

            xmlwrite.WriteStartDocument();
            xmlwrite.WriteStartElement("PurchaseOrder");
            xmlwrite.WriteElementString("IsActive", "false");
            xmlwrite.WriteElementString("PurchaseOrderNumber", order.GetPONum);
            xmlwrite.WriteElementString("Status", "o");
            xmlwrite.WriteElementString("StatusDescription", "Open");
            xmlwrite.WriteElementString("OrderDate", order.GetOrderDate.GetDateTimeFormats()[105]); //.GetDateTimeFormats()[105])
            xmlwrite.WriteElementString("ApprovedDate", order.GetOrderDate.GetDateTimeFormats()[105]); //.GetDateTimeFormats()[105])
            xmlwrite.WriteElementString("OrderNotes", "");
            xmlwrite.WriteElementString("ShipToLocation", order.GetLocation);
            xmlwrite.WriteElementString("ShipToLocationCode", order.GetLocationCode);
            xmlwrite.WriteElementString("BillToLocation", order.GetLocation);
            xmlwrite.WriteElementString("BillToLocationCode", order.GetLocationCode);
            xmlwrite.WriteElementString("CreatedBy", order.GetOrderBy);
            xmlwrite.WriteElementString("ApprovedBy", "");
            xmlwrite.WriteElementString("VendorName", "Jason Pharmaceuticals");
            xmlwrite.WriteElementString("ShippingMethod", "DNS");

            xmlwrite.WriteStartElement("Lines");

            foreach (DataRow row in table.Rows)
            {
                xmlwrite.WriteStartElement("PurchaseOrderLine");
                xmlwrite.WriteElementString("IsActive", "false");
                xmlwrite.WriteElementString("OrderNumber", order.GetPONum);
                xmlwrite.WriteElementString("LineID", row["Line"].ToString());
                xmlwrite.WriteElementString("LineStatus", "");
                xmlwrite.WriteElementString("ProductName", row["Name"].ToString());
                xmlwrite.WriteElementString("ProductSKU", row["SKU"].ToString());
                xmlwrite.WriteElementString("VendorSku", "");
                xmlwrite.WriteElementString("UnitOfMeasure", row["UOM"].ToString());
                xmlwrite.WriteElementString("OrderQty", row["Qty"].ToString());
                xmlwrite.WriteElementString("ReceivedQty", "");
                xmlwrite.WriteElementString("UnitCost", row["Cost"].ToString());
                xmlwrite.WriteElementString("VendorNotes", "");
                xmlwrite.WriteElementString("InternalNotes", "");
                xmlwrite.WriteElementString("StatusDescription", "");
                xmlwrite.WriteEndElement();
            }

            xmlwrite.WriteEndElement();//End Lines
            xmlwrite.WriteEndElement();//End Purchase Order
            xmlwrite.WriteEndDocument();
            xmlwrite.Flush();
            xmlwrite.Close();

            table.Clear();
            dataGridView1.Refresh();
            MessageBox.Show(save.FileName + " created at " + DateTime.Now, "XML Saved", MessageBoxButtons.OK);

        }
        private void Form2_Load(object sender, EventArgs e)
        {
            starttime = DateTime.Now;

            openFileDialog1.DefaultExt = "xlsx";
            openFileDialog1.Filter = "Excel Files (*.xlsx)|*.xlsx";
            openFileDialog1.InitialDirectory = "\\\\MED-FS01\\SHARE\\MWCC_Orders_for_Nav\\";
            excel.Visible = false;
            excel.DisplayAlerts = false;

            this.Text = this.ProductName + " v" + this.ProductVersion;

            //Create Column Line
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.Int32");
            column.ColumnName = "Line";
            table.Columns.Add(column);

            //Create Column OrderNum
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "OrderNum";
            table.Columns.Add(column);

            //Create Column Name
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Name";
            table.Columns.Add(column);

            //Create Column SKU
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "SKU";
            table.Columns.Add(column);

            //Create Column UOM
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "UOM";
            table.Columns.Add(column);           

            //Create Column Qty
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Qty";
            table.Columns.Add(column);

            //Create Column Cost
            column = new DataColumn();
            column.DataType = System.Type.GetType("System.String");
            column.ColumnName = "Cost";
            table.Columns.Add(column);


        }
        private void openFileDialog1_FileOk(object sender, CancelEventArgs e)
        {
            file = new FileInfo(openFileDialog1.FileName);
            textBox7.Text = file.FullName;
            openFile(file);
            /*if (region.Equals("Minnesota")) { openMNFile(file); }
            else if (region.Equals("California")) { openCAFile(file); }*/
        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }
        
        private void label5_Click(object sender, EventArgs e)
        {

        }

        
        private void menuStrip1_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            excel.Quit();
            

            Process[] processes = Process.GetProcessesByName("EXCEL");
            foreach (Process process in processes)
            {
                if (process.StartTime > starttime)
                {
                    process.Kill();
                }

            }

            Close();
            
        }
        
        private void openToolStripMenuItem_Click(object sender, EventArgs e)
        {


            /*if (region == null){

                MessageBox.Show("Region not selected.", "No region selected", MessageBoxButtons.OK);
            }
            if (region.Equals("Texas") || region.Equals("MD/PA"))
            {
                MessageBox.Show("This region is not active yet. Check back soon.", "Invalid region", MessageBoxButtons.OK);
            }
            else { openFileDialog1.ShowDialog(); }*/

            openFileDialog1.ShowDialog();
        }

        private void radioButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton1.Checked.Equals(true)) { region = "Minnesota"; }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton2.Checked.Equals(true)) { region = "California"; }
        }

        private void radioButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton3.Checked.Equals(true)) { region = "Texas"; }
        }

        private void radioButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (radioButton4.Checked.Equals(true)) { region = "MD/PA"; }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ExportXML(table);
        }

        private void exportXMLToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ExportXML(table);
        }

        private void Form2_Closing(object sender, EventArgs e)
        {
            Process[] processes = Process.GetProcessesByName("EXCEL");
            
            foreach (Process process in processes)
            {
                if (process.StartTime > starttime)
                {
                    process.Kill();
                }

            }
        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MWCC_CSharp
{
    class Order
    {
        private string _ordernum;
        private string _location;
        private string _loccode;
        private string _ponum;
        private string _orderby;
        private DateTime _orderdate;

        public string GetOrderNum
        {
            get { return _ordernum; }
        }

        public string GetLocation
        {
            get { return _location; }
        }

        public string GetLocationCode
        {
            get { return _loccode; }
        }

        public string GetPONum
        {
            get { return _ponum; }
        }

        public string GetOrderBy
        { 
            get { return _orderby; }
        }

        public DateTime GetOrderDate
        {
            get { return _orderdate; }
        }

        public void Add(string ordernum, string location, string loccode, string ponum, string orderby, string orderdate)
        {
            _ordernum = ordernum;
            _location = location;
            _loccode = loccode;
            _ponum = ponum;
            _orderby = orderby;
            if (orderdate.Length.Equals(0))
            {
                _orderdate = DateTime.Today;
            }
            else { _orderdate = DateTime.Parse(orderdate); }
            
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MWCC_CSharp
{
    class Item
    {
        private int _lineid;        
        private string _ordernum;
        private string _name;
        private string _sku;
        private string _uom;
        private string _unit;
        private string _qty;
        private string _cost;
        private string _itemout;

        public string PrintItem
        {
            get { return _itemout; }
        }

        public string GetName
        {
            get { return _name; }
        }

        public string GetSKU
        {
            get { return _sku; }
        }

        public string GetUOM
        {
            get { return _uom; }
        }

        public string GetQty
        {
            get { return _qty; }
        }
        public string GetCost
        {
            get { return _cost; }
        }

        public int GetLineID
        { 
            get { return _lineid; }
        }       

        public void Add(int lineid, string ordernum, string name, string sku, string uom, string unit, string qty, string cost)
        {
            _lineid = lineid;
            _ordernum = ordernum;
            _name = name;
            _sku = sku;
            _uom = uom;
            _unit = unit;
            _qty = qty;
            _cost = cost;
            _itemout = _lineid + "\t" + _ordernum + "\t" + _name.PadRight(40) + "\t" + _sku + "\t" + _uom + "\t" + _unit + "\t" + _qty + "\t" + _cost;

        }
    }
}
